from django.conf.urls import url
from django.urls import include,path
from . import views


app_name='administrator'

urlpatterns = [
    path('assign-admin/',views.assign,name='assign-admin')
]