from django import forms

class CreateTindakanPoliklinikForm(forms.Form):
    id_poliklinik=forms.ChoiceField(choices=[])
    nama_tindakan=forms.CharField(label='Nama Tindakan',max_length=50)
    deskripsi=forms.CharField(label='Deskripsi', required=False)
    tarif=forms.IntegerField(label='Tarif')

class UpdateTindakanPoliklinikForm(forms.Form):
    id_tindakan_poli=forms.CharField(label='ID Tindakan Poliklinik',max_length=50,required=False)
    id_poliklinik=forms.ChoiceField(label='ID Poliklinik',choices=[])
    nama_tindakan=forms.CharField(label='Nama Tindakan',max_length=50)
    deskripsi=forms.CharField(label='Deskripsi', required=False)
    tarif=forms.IntegerField(label='Tarif')