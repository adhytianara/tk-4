from django.apps import AppConfig


class TindakanPoliklinikConfig(AppConfig):
    name = 'tindakan_poliklinik'
