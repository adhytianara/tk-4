from django.conf.urls import url
from django.urls import include,path
from . import views

app_name = 'sesi_konsultasi'

urlpatterns = [
    path('display/', views.display_sesi_konsultasi,name='display-transaksi'),
    path('create/', views.create_sesi_konsultasi,name='create-sesikosultasi'),
    path('create-success/',views.create_success,name='create-success'),
    path('delete/',views.delete_sk),
    path('update/',views.update_sk),
]
