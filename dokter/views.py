from django.shortcuts import render, redirect
from django.db import connection
from .forms import AssignDokterForm, UpdateDokterForm
import json
from django.views.decorators.csrf import csrf_exempt

def assign(request):
    if "is_admin" not in request.session:
        return redirect('/unauthorized')
    if 'username' not in request.session:
        return redirect('/login')
    response = {}
    response['error'] = False
    id_dokter = get_id_dokter()
    kode_rs = get_kode_rs()
    form = AssignDokterForm(request.POST or None)
    form.fields['id_dokter'].choices=id_dokter
    form.fields['kode_rs'].choices=kode_rs
    response['form']=form
    if(request.method == "POST" and form.is_valid()):
        response['id_dokter']=request.POST['id_dokter']
        response['kode_rs']=request.POST['kode_rs']
        if(check_unique(response['id_dokter'],response['kode_rs'])):
            assign_dokter_to_rs(response['id_dokter'],response['kode_rs'])
            return redirect('/rscabang/display/')
        else:
            return redirect('/dokter/assign-fail/')
    else:
        response['error']=True
    return render(request,'assign-dokter.html',response)

def assign_fail(request):
    if 'username' in request.session and 'is_admin' in request.session:
        return render(request,'assign-fail.html')
    return redirect('/unauthorized')

def check_unique(id_dokter,kode_rs):
    cursor = connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute("select count(*) from dokter_rs_cabang where id_dokter=%s and kode_rs=%s",[id_dokter,kode_rs])
    unique = fetch(cursor)
    for test in unique:
        temp = test['count']
    if(temp==1):
        return False
    return True

def get_id_poliklinik():
    cursor=connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute("select MAX(id_poliklinik) from layanan_poliklinik;")
    id_poliklinik_response = fetch(cursor)
    for id_pol in id_poliklinik_response:
        temp = id_pol['max']
    if(temp==None):
        return 1
    return int(temp) + 1


def assign_dokter_to_rs(id_dokter,kode_rs):
    cursor = connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute("insert into dokter_rs_cabang(id_dokter,kode_rs) values(%s,%s)",[id_dokter,kode_rs])

def get_id_dokter():
    cursor = connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute("select id_dokter from dokter;")
    id_dokter_admin_response = fetch(cursor)
    id_dokter_admin = []
    for id_dokter in id_dokter_admin_response:
        idd = id_dokter['id_dokter']
        res = (idd,idd,)
        id_dokter_admin.append(res)
    return id_dokter_admin

def get_kode_rs():
        # fetch semua kode rs
    query2 = """
    select kode_rs from rs_cabang;
    """
    cursor = connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute(query2)
    kode_rs_cabang_response = fetch(cursor)
    kode_rs_cabang = []
    for kode_rs in kode_rs_cabang_response:
        kr = kode_rs['kode_rs']
        res=(kr,kr,)
        kode_rs_cabang.append(res)
    return kode_rs_cabang

def fetch(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

@csrf_exempt
def delete_dokter_rscabang(request):
    if(request.method == "POST"):
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        rs_cab = body['payload']['rs_cab']
        id_dok = body['payload']['id_dok']
        cursor = connection.cursor()
        cursor.execute("set search_path to medika_go;")
        cursor.execute("delete from dokter_rs_cabang where id_dokter =%s and kode_rs=%s",[id_dok,rs_cab])
        return redirect('/dokter/display/')
    return HttpResponse("Nothing here")

@csrf_exempt
def update_dokter(request):
    if "is_admin" not in request.session:
        return redirect('/unauthorized')
    rs_cab = request.GET.get('kode_rs',None)
    id_dok = request.GET.get('id_dokter',None)
    if(rs_cab != None and id_dok != None):
        response={}
        response['error']=False
        id_dokter = get_id_dokter()
        kode_rs = get_kode_rs()
        form = UpdateDokterForm(request.POST or None, initial={'kode_rs':rs_cab,'id_dokter':id_dok})
        form.fields['id_dokter'].choices=id_dokter
        form.fields['kode_rs'].choices=kode_rs
        response['form'] = form
    if(request.method == "POST" and form.is_valid()):
        response['id_dokter']=request.POST['id_dokter']
        response['kode_rs']=request.POST['kode_rs']
        if(check_unique(response['id_dokter'],response['kode_rs'])):
            update_dokter_to_rs(response['id_dokter'],response['kode_rs'],id_dok,rs_cab)
            return redirect('/dokter/display/')
        else:
            return redirect('/dokter/assign-fail/')
    else:
        response['error']=True
    return render(request,'update-dokter.html',response)

def update_dokter_to_rs(id_dokter,kode_rs,id_dok,rs_cab):
    cursor = connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute("update dokter_rs_cabang set kode_rs=%s, id_dokter=%s where id_dokter =%s and kode_rs=%s",[kode_rs,id_dokter,id_dok, rs_cab])

def get_dokter_rs_cabang():
    query="""
    select distinct kode_rs, string_agg(dokter.id_dokter,', '), row_number() OVER(ORDER BY kode_rs ASC) from dokter_rs_cabang, dokter
    where dokter_rs_cabang.id_dokter = dokter.id_dokter group by kode_rs order by kode_rs asc;
    """
    cursor=connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute(query)
    dokter_rs_cabang_response = fetch(cursor)
    return dokter_rs_cabang_response

def display_dokter_rs_cabang(request):
    if "username" not in request.session:
        return redirect('/login')
    dokter_rs_cabang_raw = get_dokter_rs_cabang()
    response = {}
    response['is_admin'] = "is_admin" in request.session
    response['data'] = dokter_rs_cabang_raw
    return render(request,'display-dokter.html',response)

def get_dokter_rs_cabang_view(rs_cab):
    cursor=connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute("select kode_rs, id_dokter,row_number() OVER(ORDER BY kode_rs ASC) from dokter_rs_cabang where kode_rs =%s;",[rs_cab])
    dokter_rs_cabang_response = fetch(cursor)
    return dokter_rs_cabang_response

def display_dokter_rs_cabang_delete(request):
    if "username" not in request.session:
        return redirect('/login')
    rs_cab = request.GET.get('kode_rs',None)
    dokter_rs_cabang_raw = get_dokter_rs_cabang_view(rs_cab)
    response = {}
    response['is_admin'] = "is_admin" in request.session
    response['data'] = dokter_rs_cabang_raw
    return render(request,'display-delete.html',response)

def display_dokter_rs_cabang_update(request):
    if "username" not in request.session:
        return redirect('/login')
    rs_cab = request.GET.get('kode_rs',None)
    dokter_rs_cabang_raw = get_dokter_rs_cabang_view(rs_cab)
    response = {}
    response['is_admin'] = "is_admin" in request.session
    response['data'] = dokter_rs_cabang_raw
    return render(request,'display-update.html',response)
