from django.shortcuts import render, redirect
from django.db import connection
from .forms import LoginForm,RegisterFormAdministrator, RegisterFormDokter, RegisterFormPasien

# Create your views here.
def login(request):
    if "username" in request.session:
        return redirect('/')
    response = {}
    response['error'] = False

    form = LoginForm(request.POST or None)
    response['form'] = form
    if(request.method == "POST" and form.is_valid()):
        response['username'] = request.POST['username']
        response['password'] = request.POST['password']
        if(verified(response)):
            request.session['username'] = response['username']
            if(is_admin(response['username'])):
                request.session['is_admin'] = True
            return redirect('/authentication-success')
        else:
            response['error'] = True

    return render(request,'login.html',response)

def logout(request):
    if "username" in request.session:
        request.session.flush()
        return redirect('/')
    return redirect('/')

def authentication_success(request):
    if "username" in request.session:
        return render(request,'success.html')
    else:
        return redirect('/')

def verified(data):
    query = """
    select * from pengguna;
    """
    cursor = connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute(query)

    user_data = fetch(cursor)
    for users in user_data:
        username_status = data['username'] == users['username']
        password_status = data['password'] == users['password']
        if(username_status and password_status):
            return True
    return False

def fetch(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

def is_username_taken(username):
    cursor = connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute("select username from pengguna;")
    all_registered_username = fetch(cursor)
    result = False
    for usernames in all_registered_username:
        if usernames['username'] == username:
            result = True
            break
    return result

def is_id_taken(id):
    cursor = connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute("select nomor_id from pengguna;")
    all_registered_id = fetch(cursor)
    result = False
    for ids in all_registered_id:
        if ids['nomor_id'] == id:
            result = True
            break
    return result

def is_email_taken(email):
    cursor = connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute("select email from pengguna;")
    all_registered_email = fetch(cursor)
    result = False
    for emails in all_registered_email:
        if emails['email'] == email:
            result = True
            break
    return result


def is_admin(username_input):
    # fetch semua nomor_pegawai dari admin
    query1 = """
    select username from administrator;
    """
    cursor = connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute(query1)

    username_admin_response = fetch(cursor)
    for username in username_admin_response:
        if(username_input == username['username']):
            return True
    return False

def unauthorized(request):
    if "username" in request.session:
        return render(request,'unauthorized.html')
    else:
        return redirect('/')

def register(request):
    if "username" in request.session:
        return redirect('/')
    return render(request,'register.html')

def register_administrator(request):
    if "username" in request.session:
        return redirect('/')
    response={}
    response['error'] = False
    form = RegisterFormAdministrator(request.POST or None)
    response['form'] = form
    if(request.method == 'POST' and form.is_valid()):
        username = request.POST['username']
        password = request.POST['password']
        nomor_identitas = request.POST['nomor_identitas']
        nama_lengkap = request.POST['nama_lengkap']
        tanggal_lahir = request.POST['tanggal_lahir']
        email = request.POST['email']
        alamat = request.POST['alamat']
        if(not is_username_taken(username) and (not is_email_taken(email)) and (not is_id_taken(nomor_identitas))):
            insert_pengguna(username,password,nomor_identitas,nama_lengkap,tanggal_lahir,email,alamat)
            insert_administrator(username)
            return redirect('/register/success')
        else:
            response['error'] = True
    return render(request,'register_administrator.html',response)

def insert_pengguna(username,password,nomor_identitas,nama_lengkap,tanggal_lahir,email,alamat):
    cursor = connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute("INSERT INTO PENGGUNA (email,username,password,nama_lengkap,nomor_id,tanggal_lahir,alamat) values (%s,%s,%s,%s,%s,%s,%s)",[email,username,password,nama_lengkap,nomor_identitas,tanggal_lahir,alamat])

def insert_administrator(username):
    # kode rs defaults to RSHB2001
    cursor = connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute("insert into administrator (nomor_pegawai,username,kode_rs) values(%s,%s,%s)",[get_new_nomor_pegawai(),username,'RSHB2001'])

def insert_dokter(username,no_sip,spesialisasi):
    cursor = connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute("insert into dokter (id_dokter,username,no_sip,spesialisasi) values(%s,%s,%s,%s)",[get_new_id_dokter(),username,no_sip,spesialisasi])

def insert_pasien(username,nomor_rekam_medis):
    # asuransi defaults to manulife
    cursor = connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute("insert into pasien (no_rekam_medis,username,nama_asuransi) values(%s,%s,%s)",[nomor_rekam_medis,username,"Manulife"])

def insert_alergi(nomor_rekam_medis,alergi):
    cursor = connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute("insert into alergi_pasien (no_rekam_medis,alergi) values(%s,%s)",[nomor_rekam_medis,alergi])

def register_dokter(request):
    if "username" in request.session:
        return redirect('/')
    response={}
    response['error'] = False
    form = RegisterFormDokter(request.POST or None)
    response['form'] = form
    if(request.method == 'POST' and form.is_valid()):
        print(request.POST)
        username = request.POST['username']
        password = request.POST['password']
        nomor_identitas = request.POST['nomor_identitas']
        nama_lengkap = request.POST['nama_lengkap']
        tanggal_lahir = request.POST['tanggal_lahir']
        email = request.POST['email']
        alamat = request.POST['alamat']
        nomor_sip = request.POST['nomor_sip']
        spesialisasi = request.POST['spesialisasi']
        if(not is_username_taken(username) and (not is_email_taken(email)) and (not is_id_taken(nomor_identitas))):
            insert_pengguna(username,password,nomor_identitas,nama_lengkap,tanggal_lahir,email,alamat)
            insert_dokter(username,nomor_sip,spesialisasi)
            return redirect('/register/success')
        else:
            request['error'] = True
    return render(request,'register_dokter.html',response)

def register_pasien(request):
    if "username" in request.session:
        return redirect('/')
    response={}
    response['error'] = False
    form = RegisterFormPasien(request.POST or None)
    response['form'] = form
    if(request.method == 'POST' and form.is_valid()):
        print(request.POST)
        username = request.POST['username']
        password = request.POST['password']
        nomor_identitas = request.POST['nomor_identitas']
        nama_lengkap = request.POST['nama_lengkap']
        tanggal_lahir = request.POST['tanggal_lahir']
        email = request.POST['email']
        alamat = request.POST['alamat']
        alergi = request.POST['alergi']
        nomor_rekam_medis = get_new_no_rekam_medis()
        if(not is_username_taken(username) and (not is_email_taken(email)) and (not is_id_taken(nomor_identitas))):
            insert_pengguna(username,password,nomor_identitas,nama_lengkap,tanggal_lahir,email,alamat)
            insert_pasien(username,nomor_rekam_medis)
            insert_alergi(nomor_rekam_medis,alergi)
            return redirect('/register/success')
        else:
            response['error'] = True
    return render(request,'register_pasien.html',response)


def register_success(request):
    return render(request,'register-success.html')

def get_new_nomor_pegawai():
    cursor = connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute("select nomor_pegawai from administrator;")
    all_nomor_pegawai = fetch(cursor)
    temp = []
    for no_pegawai in all_nomor_pegawai:
        temp.append(no_pegawai['nomor_pegawai'])
    result = int(max(temp)) + 1
    return result

def get_new_id_dokter():
    cursor = connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute("select id_dokter from dokter;")
    all_id_dokter = fetch(cursor)
    temp = []
    for id_dokter in all_id_dokter:
        temp.append(id_dokter['id_dokter'])
    result = int(max(temp)) + 1
    return result

def get_new_no_rekam_medis():
    cursor = connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute("select no_rekam_medis from pasien;")
    all_no_rekam_medis = fetch(cursor)
    temp = []
    anynumber = False
    result = ""
    for no_rekam_medis in all_no_rekam_medis:
        if no_rekam_medis['no_rekam_medis'].isnumeric():
            anynumber = True
            result = no_rekam_medis['no_rekam_medis']
            temp.append(int(no_rekam_medis['no_rekam_medis']))
    if(anynumber):
        return int(max(temp)) + 1
    else:
        return 0
