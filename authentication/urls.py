from django.conf.urls import url
from django.urls import include,path
from . import views

app_name='authentication'

urlpatterns = [
    path('login/',views.login,name='login'),
    path('authentication-success/',views.authentication_success,name='authentication-success'),
    path('logout/',views.logout,name='logout'),
    path('unauthorized/',views.unauthorized,name='unauthorized'),
    path('register/',views.register,name='register'),
    path('register/administrator',views.register_administrator,name='register-administrator'),
    path('register/success',views.register_success,name='register-success'),
    path('register/dokter',views.register_dokter,name='register-dokter'),
    path('register/pasien',views.register_pasien,name='register-pasien')
]