from django import forms

class LoginForm(forms.Form):
    username=forms.CharField(label='username',max_length=20)
    password=forms.CharField(widget=forms.PasswordInput)

class RegisterFormAdministrator(forms.Form):
    username= forms.CharField(label='username',max_length=50)
    password= forms.CharField(label='password',widget=forms.PasswordInput)
    nomor_identitas=forms.CharField(label='Nomor Identitas',max_length=50)
    nama_lengkap=forms.CharField(label='Nama Lengkap',max_length=50)
    tanggal_lahir = forms.DateTimeField()
    email=forms.EmailField(label="Email",max_length=50)
    alamat= forms.CharField(label="alamat")

class RegisterFormDokter(forms.Form):
    username= forms.CharField(label='username')
    password= forms.CharField(label='password',widget=forms.PasswordInput)
    nomor_identitas=forms.CharField(label='Nomor Identitas')
    nama_lengkap=forms.CharField(label='Nama Lengkap')
    tanggal_lahir = forms.DateTimeField()
    email=forms.CharField(label="Email")
    alamat= forms.CharField(label="alamat")
    nomor_sip=forms.CharField(label='Nomor SIP')
    spesialisasi = forms.CharField(label='spesialisasi')

class RegisterFormPasien(forms.Form):
    username= forms.CharField(label='username')
    password= forms.CharField(label='password',widget=forms.PasswordInput)
    nomor_identitas=forms.CharField(label='Nomor Identitas')
    nama_lengkap=forms.CharField(label='Nama Lengkap')
    tanggal_lahir = forms.DateTimeField()
    email=forms.CharField(label="Email")
    alamat= forms.CharField(label="alamat")
    alergi = forms.CharField(label="Alergi (pisahkan dengan koma bila lebih dari satu) ",required=False)
