from django.conf.urls import url
from django.urls import include,path
from . import views

app_name = 'rs_cabang'

urlpatterns = [
    path('create/',views.create_rscabang),
    path('display/',views.display_rs),
    path('delete_rs/',views.delete_rs),
    path('update_rs/',views.update_rs),
]
