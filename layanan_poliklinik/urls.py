from django.conf.urls import url
from django.urls import include,path
from layanan_poliklinik import views

app_name='layanan_poliklinik'

urlpatterns = [
    path('create/',views.create_layanan_poliklinik,name='create-layanan_poliklinik'),
    path('display/',views.display_layanan_poliklinik,name='display-layanan_poliklinik'),
    path('display-jadwal/',views.display_jadwal_poliklinik,name='display-jadwal_poliklinik'),
    path('delete/',views.delete_layanan_poliklinik,name='delete-layanan'),
    path('delete-jadwal/',views.delete_jadwal_poliklinik,name='delete-jadwal'),
    path('update-layanan/',views.update_layanan_poliklinik,name='update-layanan'),
    path('update-jadwal/',views.update_jadwal_poliklinik,name='update-jadwal')
]