from django.conf.urls import url
from django.urls import include,path
from . import views

app_name='resep'

urlpatterns = [
    path('create/',views.create_resep,name='create-resep'),
    path('create-success/',views.create_success,name='create-success'),
    path('display/',views.display_resep,name='display-resep'),
    path('delete-resep/',views.delete_resep,name='delete-resep'),
    path('create-resep-api/',views.create_resep_api,name='create-resep-api'),
]