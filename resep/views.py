from django.shortcuts import render, redirect, HttpResponse
from django.db import connection
# Create your views here.
from .forms import CreateResepForm
import time
from django.views.decorators.csrf import csrf_exempt
import json

def create_resep(request):
    # kalo dia bukan admin, tampilin unauthorized
    if "is_admin" not in request.session:
        return render(request,'unauthorized.html')
    response = {}
    response['error'] = False
    id_konsultasi = get_id_konsultasi()
    id_transaksi = get_id_transaksi()
    kode_obat = get_kode_obat()
    form = CreateResepForm(request.POST or None)
    form.fields['id_konsultasi'].choices = id_konsultasi
    form.fields['id_transaksi'].choices = id_transaksi
    form.fields['kode_obat'].choices=kode_obat
    response['form'] = form
    return render(request,'create-resep.html',response)

def create_success(request):
    if 'username' in request.session and 'is_admin' in request.session:
        return render(request,'create-success.html')
    return redirect('/unauthorized')

def fetch(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

def get_id_konsultasi():
    query="""
    select id_konsultasi from sesi_konsultasi;
    """
    cursor=connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute(query)
    id_konsultasi_response = fetch(cursor)
    res=[]
    for id_konsultasi in id_konsultasi_response:
        temp = id_konsultasi['id_konsultasi']
        res.append((temp,temp,))
    return res

def get_id_transaksi():
    query="""
    select id_transaksi from transaksi;
    """
    cursor=connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute(query)
    id_transaksi_response = fetch(cursor)
    res=[]
    for id_transaksi in id_transaksi_response:
        temp = id_transaksi['id_transaksi']
        res.append((temp,temp,))
    return res

def get_kode_obat():
    query="""
    select kode from obat;
    """
    cursor=connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute(query)
    kode_obat_response = fetch(cursor)
    res=[]
    for kode_obat in kode_obat_response:
        temp = kode_obat['kode']
        res.append((temp,temp,))
    return res

def get_resep():
    query="""
    select resep.no_resep,resep.id_konsultasi,resep.total_harga,resep.id_transaksi,string_agg(daftar_obat.kode_obat,', ')
    from resep join daftar_obat
    on resep.no_resep = daftar_obat.no_resep
    group by resep.no_resep
    order by resep.no_resep asc;
    """
    cursor=connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute(query)
    resep_response = fetch(cursor)
    return resep_response

def display_resep(request):
    if "username" not in request.session:
        return redirect('/login')
    resep_raw = get_resep()
    response = {}
    response['is_admin'] = "is_admin" in request.session
    response['data'] = resep_raw
    return render(request,'display-resep.html',response)

@csrf_exempt
def delete_resep(request):
    if request.method == "POST":
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        id_resep = body['payload']
        cursor = connection.cursor()
        cursor.execute("set search_path to medika_go;")
        cursor.execute("delete from resep where no_resep=%s;",[id_resep])
        return redirect("/resep/display")
    return HttpResponse("Nothing here")

def get_new_no_resep():
    cursor = connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute("select no_resep from resep;")
    no_resep_response = fetch(cursor)
    current_no_resep = []
    for no_resep in no_resep_response:
        current_no_resep.append(no_resep['no_resep'])
    return int(max(current_no_resep))+1

@csrf_exempt
def create_resep_api(request):
    if request.method == "POST":
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        print(body['payload'])
        id_konsultasi = body['payload']['id_konsultasi']
        id_transaksi = body['payload']['id_transaksi']
        kode_obat = body['payload']['kode_obat']
        no_resep = get_new_no_resep()
        cursor = connection.cursor()
        cursor.execute("set search_path to medika_go;")
        cursor.execute("insert into resep (id_konsultasi,no_resep,total_harga,id_transaksi) values(%s,%s,%s,%s);",[id_konsultasi,no_resep,0,id_transaksi])
        for kode in kode_obat:
            create_daftar_obat(no_resep,kode)
        return redirect("/resep/display")
    return HttpResponse("Nothing here")

def create_daftar_obat(no_resep, kode_obat):
    # dosis defaults to 500 grams
    # aturan defaults to 3x sehari
    cursor = connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute("insert into daftar_obat (no_resep,kode_obat,dosis,aturan) values(%s,%s,%s,%s)",[no_resep,kode_obat,"500 gram","3x sehari"])